import os
import matplotlib.pyplot as plt
import matplotlib
import numpy as np

root = '/home/nhatminh2947/working/projects/backup/lab7_stat'
stat = None


for i in range(200):
    name = 'stat_{}.csv'.format(i)
    path = os.path.join(root, name)

    if os.path.exists(path):
        if stat is None:
            stat = np.loadtxt(path, delimiter=',')
        else:
            stat = np.vstack((stat, np.loadtxt(path, delimiter=',')))

plt.figure(figsize=(12, 5))
plt.plot(stat[:, 0], c='C0')
plt.title('Training Episode Score')
plt.xlabel('Episodes')
plt.ylabel('Score')
plt.tight_layout()
plt.savefig('./stat.png')

