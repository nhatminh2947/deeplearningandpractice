import torch
from lab5 import Model, Const, dataset

IO_DIM = 30
EMBEDDING_DIM = 512
HIDDEN_DIM = 512
CONDITION_DIM = 8
LATENT_DIM = 32


def decode_inference(decoder, z, target_condition_embedded, max_len=20):
    word_processing = dataset.WordProcessing()
    z = z.view(1, 1, -1)

    hidden = decoder.initHidden(z, target_condition_embedded)
    input = torch.tensor(word_processing.char2int(word_processing.SOS_TOKEN)).to(Const.device)
    target_vocab_size = decoder.output_dim

    outputs = torch.zeros(max_len, 1, target_vocab_size).to(Const.device)

    for t in range(1, max_len):
        output, hidden = decoder(input, hidden)
        outputs[t] = output
        top1 = torch.argmax(output, dim=1)
        input = top1

    return outputs


def count_correct_generate(english_tense, words=None):
    if words is None:
        return False

    data = english_tense.data.reshape(-1, 4)
    max_count = 0

    for tenses in data:
        count = 0
        for i, word in enumerate(tenses):
            count += (word == words[i])

        max_count = max(max_count, count)

    return max_count


def word_generation(path):
    word_processing = dataset.WordProcessing()
    checkpoint = torch.load(path, map_location='cpu')
    model = Model.EncoderDecoder(IO_DIM, EMBEDDING_DIM, HIDDEN_DIM, CONDITION_DIM, LATENT_DIM).to(Const.device)
    model.load_state_dict(checkpoint['model'])

    model.eval()

    english_tense = dataset.EnglishTenseDataset()
    generated_4 = 0

    while generated_4 != 2:
        noise = model.encoder.sample_z()
        words = []
        for i in range(len(english_tense.tenses)):
            condition = torch.tensor([i]).long().to(Const.device)
            target_condition_embedded = model.condition_embedding(condition).view(1, 1, -1)
            outputs = decode_inference(model.decoder, noise, target_condition_embedded)
            outputs = outputs[1:].view(-1, outputs.shape[-1])
            prediction = torch.argmax(torch.softmax(outputs, dim=1), dim=1)
            prediction = word_processing.tensor2word(prediction)
            # print('{:20s} : {}'.format(english_tense.tenses[i], prediction))
            words.append(prediction)

        count = count_correct_generate(english_tense, words)

        if count >= 3:
            print(words)

            if count == 4:
                generated_4 += 1


if __name__ == '__main__':
    word_generation('./best_model.pth')
