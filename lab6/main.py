from lab6.model import *
from lab6.trainer import Trainer
from torchvision import datasets, transforms
import torch


if __name__ == '__main__':
    mnist_dataset = datasets.MNIST(root='./data', train=True, download=True,
                                   transform=transforms.Compose([transforms.ToTensor(),
                                                                 transforms.Normalize((0.1307,), (0.3081,))]))

    trainer = Trainer(Generator(), FrontEnd(), Discriminator(), Q())
    metrics = trainer.train(dataset=mnist_dataset)

    torch.save(trainer, './trainer_model.pth')
    torch.save(metrics, './metrics.pth')
