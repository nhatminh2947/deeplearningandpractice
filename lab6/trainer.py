import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import DataLoader
import time
from torchvision.utils import save_image
import os

cuda = torch.device('cuda:0')


class Trainer:
    def __init__(self, generator, front_end, discriminator, q):
        self.generator = generator.cuda()
        self.front_end = front_end.cuda()
        self.discriminator = discriminator.cuda()
        self.q = q.cuda()
        self.noise_size = 64
        self.c_size = 10

        self.criterion_discriminator = nn.BCELoss().cuda()
        self.criterion_q = nn.CrossEntropyLoss().cuda()

        self.optim_d = optim.Adam(list(self.front_end.parameters()) + list(self.discriminator.parameters()),
                                  lr=0.0002, betas=(0.5, 0.99))

        self.optim_g = optim.Adam(list(self.generator.parameters()) + list(self.q.parameters()),
                                  lr=0.001, betas=(0.5, 0.99))

    def _noise_sample(self, batch_size):
        # gen condition c
        idx = np.random.randint(self.c_size, size=batch_size)

        c = np.zeros((batch_size, self.c_size))
        c[range(batch_size), idx] = 1.0

        # gen torch (batch, #noise, 1, 1)
        z = torch.cat([
            torch.empty((batch_size, self.noise_size - self.c_size),
                        dtype=torch.float,
                        device=cuda).uniform_(-1.0, 1.0),
            torch.tensor(c, dtype=torch.float, device=cuda)
        ], 1).view(-1, self.noise_size, 1, 1)

        return z.cuda(), torch.tensor(idx, dtype=torch.long).cuda()

    def train(self, dataset, batch_size=64, epochs=80):
        self.generator.train()
        self.front_end.train()
        self.discriminator.train()
        self.q.train()

        idx = np.arange(10).repeat(10)
        one_hot = np.zeros((100, 10))
        one_hot[range(100), idx] = 1
        one_hot = torch.tensor(one_hot, dtype=torch.float).cuda()
        fix_noise = torch.randn((100, 54), dtype=torch.float).uniform_(-1, 1).cuda()

        dataloader = DataLoader(dataset, batch_size=batch_size, shuffle=True, num_workers=1)
        metrics = list()
        label = torch.ones((batch_size, 1), dtype=torch.float, device=cuda)
        running_time = 0.0
        for epoch in range(epochs):
            for i, batch_data in enumerate(dataloader, 0):
                start = time.time()
                self.optim_d.zero_grad()

                x, _ = batch_data

                current_batch_size = x.size(0)
                label.resize_((current_batch_size, 1))

                real_x = x.cuda()
                # print('front_end fe_real_x.shape: {}'.format(fe_real_x.shape))

                probs_real = self.discriminator(self.front_end(real_x))
                # print('discriminator real_out.shape: {}'.format(real_out.shape))
                label.fill_(1.0)

                loss_d_real = self.criterion_discriminator(probs_real, label)
                loss_d_real.backward()

                # Feed fake image from generator
                # ====================================================================
                z, idx = self._noise_sample(current_batch_size)

                fake_x = self.generator(z)
                # print('generator z.shape: {}'.format(fake_x.shape))
                probs_fake = self.discriminator(self.front_end(fake_x.detach()))

                label.fill_(0.0)
                loss_d_fake = self.criterion_discriminator(probs_fake, label)
                loss_d_fake.backward()

                self.optim_d.step()
                loss_D = loss_d_real + loss_d_fake

                # Generator and Q part
                # ====================================================================
                self.optim_g.zero_grad()

                fe_out = self.front_end(fake_x)
                probs_fake = self.discriminator(fe_out)
                label.fill_(1.0)

                loss_g_reconstruct = self.criterion_discriminator(probs_fake, label)
                # print('front_end fe_out.shape: {}'.format(fe_out.shape))
                c = self.q(fe_out)

                loss_q = self.criterion_q(c, idx)

                loss_g = loss_g_reconstruct + loss_q
                loss_g.backward()

                self.optim_g.step()

                fake_x = self.generator(z)
                probs_fake_after = self.discriminator(self.front_end(fake_x.detach()))

                if (i + 1) % 100 == 0:
                    print('Running time: {}'.format(running_time / 100))
                    print('Epoch/Batch:{:4d}/{:4d}, loss G: {:4.2f}. loss D: {:4.2f} loss Q: {:4.2f}'.format(
                        epoch + 1, i + 1, loss_g.item(), loss_D.item(), loss_q.item()
                    ))

                    running_time = 0.0

                    idx = np.arange(self.c_size)
                    c = np.zeros((self.c_size, self.c_size))
                    c[range(self.c_size), idx] = 1.0

                    # gen torch (#cluster, #noise, 1, 1) with same noise
                    z = torch.cat([fix_noise, one_hot], 1).view(-1, self.noise_size, 1, 1)

                    x_save = self.generator(z.cuda())
                    save_img(x_save, './images', epoch)
                    save_img(x_save, './images', 'result')

                real = torch.mean(probs_real).item()
                fake_before = torch.mean(probs_fake).item()
                fake_after = torch.mean(probs_fake_after).item()

                metrics.append((loss_g.item(), loss_D.item(), loss_q.item(), real, fake_before, fake_after))

                end = time.time()
                running_time += (end - start)

            # if (epoch + 1) % 10 == 0:
            torch.save({
                'metrics': metrics,
                'discriminator': self.discriminator.state_dict(),
                'q': self.q.state_dict(),
                'generator': self.generator.state_dict(),
                'front_end': self.front_end.state_dict()
            }, './models/trainer_{}.pth'.format(epoch))

        return metrics


def save_img(x, path, epoch):
    if not os.path.isdir(path):
        os.mkdir(path)
    save_image(x.data, os.path.join(path, 'image_{}.png'.format(epoch)), nrow=10)
