import numpy as np
import matplotlib.pyplot as plt
import torch


def smoothing_data(data, smoothing_weight=0.9):
    last = data[0]
    for i, x in enumerate(data):
        data[i] = last * smoothing_weight + (1 - smoothing_weight) * x
        last = data[i]

    return data


checkpoint = torch.load('./models/trainer_79.pth')
metrics = np.array(checkpoint['metrics'])

real_data = smoothing_data(metrics[:, 3])
fake_before_data = smoothing_data(metrics[:, 4])
fake_after_data = smoothing_data(metrics[:, 5])


plt.figure(figsize=(12, 4))
plt.title('Probability curve')
plt.plot(real_data, label='P(real data)')
plt.plot(fake_before_data, label='P(fake before update)')
plt.plot(fake_after_data, label='P(fake after update)')
plt.xlabel('Every 100 batch')
plt.ylabel('Probability')
plt.legend()

plt.savefig('./images/prob_curve.png')

loss_g = smoothing_data(metrics[:, 0])
loss_d = smoothing_data(metrics[:, 1])
loss_q = smoothing_data(metrics[:, 2])

plt.figure(figsize=(12, 4))
plt.title('Loss curve\nGenerator, Discriminator, Q')
plt.plot(loss_g, label='loss G')
plt.plot(loss_d, label='loss D')
plt.plot(loss_q, label='loss Q')
plt.xlabel('Every 100 batch')
plt.ylabel('Loss')
plt.legend()

plt.savefig('./images/loss_curve.png')
