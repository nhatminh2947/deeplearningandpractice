import numpy as np
import torch
from torchvision.utils import save_image
from lab6.model import *

cuda = torch.device('cuda:0')

c_size = 10
noise_size = 64

def generate_result():
    idx = np.array([], dtype=np.int)

    for i in range(c_size):
        idx = np.append(idx, np.arange(10))

    one_hot = np.zeros((100, 10))
    one_hot[range(100), idx] = 1
    one_hot = torch.tensor(one_hot, dtype=torch.float).cuda()

    fixed_noise = torch.zeros(0, noise_size - c_size, device=cuda, dtype=torch.float)

    for i in range(c_size):
        random_noise = torch.randn((1, noise_size - c_size), dtype=torch.float, device=cuda).uniform_(-1.0, 1.0).expand(10, -1)
        fixed_noise = torch.cat([fixed_noise, random_noise], dim=0)

    z = torch.cat([fixed_noise, one_hot], 1).view(-1, noise_size, 1, 1)


    checkpoint = torch.load('./models/trainer_79.pth')

    generator = Generator()
    generator.load_state_dict(checkpoint['generator'])
    generator = generator.cuda()

    x = generator(z.cuda())
    save_image(x.data, './images/result.png', nrow=10)


def generate_from_idx(index):
    idx = np.arange(10)
    idx.fill(index)

    one_hot = np.zeros((c_size, c_size))
    one_hot[range(c_size), idx] = 1
    one_hot = torch.tensor(one_hot, dtype=torch.float).cuda()

    z = torch.cat([torch.randn((10, noise_size - c_size), dtype=torch.float, device=cuda).uniform_(-1.0, 1.0), one_hot], 1).view(-1, noise_size, 1, 1)

    checkpoint = torch.load('./models/trainer_79.pth')

    generator = Generator()
    generator.load_state_dict(checkpoint['generator'])
    generator = generator.cuda()

    x = generator(z.cuda())
    save_image(x.data, './images/generate_from_{}.png'.format(index), nrow=10)


generate_from_idx(9)