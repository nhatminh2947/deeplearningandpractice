from torch.utils.data import Dataset
from torchvision import transforms
from PIL import Image
from enum import Enum
import torch
import pandas as pd
import numpy as np


class Mode(Enum):
    TRAIN = 0
    TEST = 1


def get_data(mode):
    if mode == Mode.TRAIN:
        img = pd.read_csv('train_img.csv', header=None, names=['image_path'])
        label = pd.read_csv('train_label.csv', header=None, names=['label'])

        train_data = pd.concat([img, label], axis=1)
        train_data = train_data.sample(frac=1).reset_index(drop=True)

        img = train_data['image_path'].astype(str)
        label = train_data['label'].astype(str)

        return np.squeeze(img.values), np.squeeze(label.values)
    elif mode == Mode.TEST:
        img = pd.read_csv('test_img.csv').astype(str)
        label = pd.read_csv('test_label.csv').astype(str)
        return np.squeeze(img.values), np.squeeze(label.values)


class RetinopathyDataset(Dataset):
    def __init__(self, root='/home/cgilab/working/DeepLearningAndPractice/data/', mode=Mode.TRAIN):
        if mode == Mode.TRAIN:
            self.transform = transforms.Compose([
                transforms.RandomHorizontalFlip(),
                transforms.RandomVerticalFlip(),
                transforms.ToTensor(),
                transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
            ])
        else:
            self.transform = transforms.Compose([
                transforms.ToTensor(),
                transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
            ])

        self.root = root
        self.image_names, self.labels = get_data(mode)
        self.mode = mode
        print('Found %d images' % (len(self.image_names)))

    def __len__(self):
        return len(self.image_names)

    def __getitem__(self, index):
        image_path = self.root + self.image_names[index] + '.jpeg'
        img_as_img = Image.open(image_path)

        img_as_tensor = self.transform(img_as_img)

        return img_as_tensor, torch.from_numpy(np.array(self.labels[index], dtype=int))


if __name__ == '__main__':
    test_dataset = RetinopathyDataset(mode=Mode.TRAIN)
    img_names, labels = get_data(Mode.TRAIN)
    for idx, (img, label) in enumerate(test_dataset):
        img.save('/home/cgilab/working/DeepLearningAndPractice/lab3_preprocessed_image/' + img_names[idx] + '.jpeg')
    # torch.save(train_dataset, 'preprocessed_train_dataset.pth')