import torchvision.models as models
import torch
import torch.nn as nn
import torch.optim as optim
import math
import matplotlib.pyplot as plt
from torch.autograd import Variable
from torch.utils.data import DataLoader
from lab3 import dataset

num_epochs = 10
batch_size = 8
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
print("Device: {}".format(device))


def find_lr(init_value=1e-8, final_value=10., beta=0.98):
    train_loader = DataLoader(dataset=dataset.RetinopathyDataset(mode=dataset.Mode.TRAIN), batch_size=batch_size)
    model = models.resnet50(pretrained=True)
    model.to(device)
    optimizer = optim.SGD(model.parameters(), lr=1e-2, momentum=0.9, weight_decay=5e-4)
    # optimizer = optim.Adam(resnet18.parameters(), lr=1e-2)
    criterion = nn.CrossEntropyLoss()

    num = len(train_loader) - 1
    mult = (final_value / init_value) ** (1 / num)
    lr = init_value
    optimizer.param_groups[0]['lr'] = lr
    avg_loss = 0.
    best_loss = 0.
    batch_num = 0
    losses = []
    log_lrs = []
    for data in train_loader:
        batch_num += 1
        print('Batch_num: {}'.format(batch_num))
        # As before, get the loss for this mini-batch of inputs/outputs
        inputs, labels = data
        inputs, labels = Variable(inputs).to(device), Variable(labels).to(device)
        optimizer.zero_grad()
        outputs = model(inputs)
        loss = criterion(outputs, labels)
        # Compute the smoothed loss
        avg_loss = beta * avg_loss + (1 - beta) * loss.item()
        smoothed_loss = avg_loss / (1 - beta ** batch_num)
        # Stop if the loss is exploding
        if batch_num > 1 and smoothed_loss > 4 * best_loss:
            return log_lrs, losses
        # Record the best loss
        if smoothed_loss < best_loss or batch_num == 1:
            best_loss = smoothed_loss
        # Store the values
        losses.append(smoothed_loss)
        log_lrs.append(math.log10(lr))
        # Do the SGD step
        loss.backward()
        optimizer.step()
        # Update the lr for the next step
        lr *= mult
        optimizer.param_groups[0]['lr'] = lr
    return log_lrs, losses


if __name__ == '__main__':
    logs, losses = find_lr()
    plt.plot(logs[10:-5], losses[10:-5])
    plt.show()

