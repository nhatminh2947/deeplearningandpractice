import matplotlib.pyplot as plt
import pandas as pd


def count_label():
    train_labels = pd.read_csv('./train_label.csv', header=None, names=['label'])
    train_labels.hist()
    plt.show()
    print(train_labels['label'].value_counts())
    print(train_labels['label'].value_counts(normalize=True))

    test_labels = pd.read_csv('./test_label.csv', header=None, names=['label'])
    test_labels.hist()
    plt.show()
    print(test_labels['label'].value_counts())
    print(test_labels['label'].value_counts(normalize=True))


if __name__ == '__main__':
    count_label()