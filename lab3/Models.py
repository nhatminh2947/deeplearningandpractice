import torch.nn as nn
import torch
import math
from torch.autograd import Variable
from sklearn.metrics import confusion_matrix


def train(epoch, train_loader, model, criterion, optimizer, device, log_rate=100):
    model.train()
    correct = 0
    total_loss = 0
    for i, (inputs, targets) in enumerate(train_loader):
        inputs = Variable(inputs).to(device)
        targets = Variable(targets).to(device)

        outputs = model(inputs)
        loss = criterion(outputs, targets)

        _, predicted = torch.max(outputs.data, 1)
        correct += (predicted == targets).sum().item()
        total_loss += loss.item()

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        if i % log_rate == 0:
            print('[Epoch {}] [{:6.2f}%]: Training loss: {:.4f} | Accuracy: {:.4f}%'
                  .format(epoch + 1, 100. * i / len(train_loader),
                          total_loss / (i + 1),
                          (correct / ((i + 1) * train_loader.batch_size)) * 100))
    print('[Epoch {}] [{:6.2f}%]: Training loss: {:.4f} | Accuracy: {:.4f}%'
          .format(epoch + 1, 100.00,
                  total_loss / (i + 1),
                  (correct / ((i + 1) * train_loader.batch_size)) * 100))

    return total_loss / len(train_loader), (correct / len(train_loader.dataset)) * 100


def validate(epoch, test_loader, model, criterion, device, log_rate=10):
    model.eval()
    correct = 0
    total_loss = 0
    y_pred = []
    y_true = []
    for i, (inputs, targets) in enumerate(test_loader):
        with torch.no_grad():
            inputs = Variable(inputs).to(device)
            targets = Variable(targets).to(device)
            outputs = model(inputs)
            loss = criterion(outputs, targets)
            total_loss += loss.item()

            _, predicted = torch.max(outputs.data, 1)
            y_true.extend(targets.cpu().data.numpy())
            y_pred.extend(predicted.cpu().data.numpy())

            # print(y_true)
            # print(y_pred)
            correct += (predicted == targets).sum().item()

        if i % log_rate == 0:
            print('[Epoch {}] [{:6.2f}%]: Testing loss: {:.4f} | Accuracy: {:.4f}%'
                  .format(epoch + 1,
                          100. * i / len(test_loader),
                          total_loss / (i + 1),
                          (correct / ((i + 1) * test_loader.batch_size)) * 100))

    print('[Epoch {}] [{:6.2f}%]: Testing loss: {:.4f} | Accuracy: {:.4f}%'
          .format(epoch + 1,
                  100.00,
                  total_loss / len(test_loader),
                  (correct / len(test_loader.dataset) * 100)))

    cm = confusion_matrix(y_true, y_pred)

    return total_loss / len(test_loader), 100. * correct / len(test_loader.dataset), cm


def conv3x3(in_planes, out_planes, stride=1):
    return nn.Conv2d(in_planes, out_planes, kernel_size=3, stride=stride, padding=1, bias=False)


def conv1x1(in_planes, out_planes, stride=1):
    return nn.Conv2d(in_planes, out_planes, kernel_size=1, stride=stride, bias=False)


class BasicBlock(nn.Module):
    expansion = 1

    def __init__(self, in_channels, channels, stride=1, downsample=None):
        super(BasicBlock, self).__init__()
        self.conv1 = conv3x3(in_channels, channels, stride)
        self.bn1 = nn.BatchNorm2d(channels)
        self.relu = nn.ReLU(inplace=True)
        self.conv2 = conv3x3(channels, channels)
        self.bn2 = nn.BatchNorm2d(channels)
        self.downsample = downsample
        self.stride = stride

    def forward(self, x):
        identity = x

        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)

        out = self.conv2(out)
        out = self.bn2(out)

        if self.downsample is not None:
            identity = self.downsample(x)

        out += identity
        out = self.relu(out)

        return out


class Bottleneck(nn.Module):
    expansion = 4

    def __init__(self, in_channels, channels, stride=1, downsample=None):
        super(Bottleneck, self).__init__()
        self.conv1 = conv1x1(in_channels, channels)
        self.bn1 = nn.BatchNorm2d(channels)
        self.conv2 = conv3x3(channels, channels, stride)
        self.bn2 = nn.BatchNorm2d(channels)
        self.conv3 = conv1x1(channels, channels * self.expansion)
        self.bn3 = nn.BatchNorm2d(channels * self.expansion)
        self.relu = nn.ReLU(inplace=True)
        self.downsample = downsample
        self.stride = stride

    def forward(self, x):
        identity = x

        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)

        out = self.conv2(out)
        out = self.bn2(out)
        out = self.relu(out)

        out = self.conv3(out)
        out = self.bn3(out)

        if self.downsample is not None:
            identity = self.downsample(x)

        out += identity
        out = self.relu(out)

        return out


class ResNet(nn.Module):
    def __init__(self, block, layers, zero_init_residual=False):
        super(ResNet, self).__init__()
        self.in_channels = 64
        self.conv1 = nn.Conv2d(3, 64, kernel_size=7, stride=2, padding=3,
                               bias=False)
        self.bn1 = nn.BatchNorm2d(64)
        self.relu = nn.ReLU(inplace=True)
        self.maxpool = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)
        self.layer1 = self.build_block(block, 64, layers[0])
        self.layer2 = self.build_block(block, 128, layers[1], stride=2)
        self.layer3 = self.build_block(block, 256, layers[2], stride=2)
        self.layer4 = self.build_block(block, 512, layers[3], stride=2)
        self.avgpool = nn.AdaptiveAvgPool2d((1, 1))
        self.fc = nn.Linear(512 * block.expansion, 5)

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
            elif isinstance(m, nn.BatchNorm2d):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)

        if zero_init_residual:
            for m in self.modules():
                if isinstance(m, Bottleneck):
                    nn.init.constant_(m.bn3.weight, 0)
                elif isinstance(m, BasicBlock):
                    nn.init.constant_(m.bn2.weight, 0)

    def build_block(self, block, channels, blocks, stride=1):
        downsample = None
        if stride != 1 or self.in_channels != channels * block.expansion:
            downsample = nn.Sequential(
                conv1x1(self.in_channels, channels * block.expansion, stride),
                nn.BatchNorm2d(channels * block.expansion),
            )

        layers = [block(self.in_channels, channels, stride, downsample)]

        self.in_channels = channels * block.expansion
        for _ in range(1, blocks):
            layers.append(block(self.in_channels, channels))

        return nn.Sequential(*layers)

    def forward(self, x):
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.relu(x)
        x = self.maxpool(x)

        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)
        x = self.layer4(x)

        x = self.avgpool(x)
        x = x.view(x.size(0), -1)
        x = self.fc(x)

        return x


def resnet18():
    return ResNet(BasicBlock, [2, 2, 2, 2])


def resnet50():
    return ResNet(Bottleneck, [3, 4, 6, 3])
