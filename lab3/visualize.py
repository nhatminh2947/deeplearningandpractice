import matplotlib.pyplot as plt
import torch
import numpy as np
import torch.nn as nn
import torchvision.models as models
from torch.utils.data import DataLoader, RandomSampler
from lab3 import dataset
from lab3 import Models
from enum import Enum


class Criteria(Enum):
    ACCURACY = 'Accuracy'
    LOSS = 'Loss'


def plot_comparision(path, name, pretrain, find_lr, criteria):
    checkpoint = torch.load(path)

    train = []
    test = []
    if criteria == Criteria.LOSS:
        (train, test) = checkpoint['loss_logger']
    elif criteria == Criteria.ACCURACY:
        (train, test) = checkpoint['accuracy_logger']

    line_name = name + '_' + ('without' if not pretrain else 'with') + '_pretrain_' \
                + ('without' if not find_lr else 'with') \
                + '_find_lr'

    plt.plot(train[-20:], label=line_name + '_train_accuracy')
    plt.plot(test[-20:], label=line_name + '_test')


# ./best_models/pretrain/find_lr/continue_training/resnet50.pth
# ./best_models/pretrain/find_lr/continue_training/resnet50.pth


def plot_confusion_matrix(path):
    checkpoint = torch.load(path)
    resnet_models = [Models.resnet18(), Models.resnet50()]
    resnet_fcs = [nn.modules.Linear(in_features=512, out_features=5, bias=True),
                  nn.modules.Linear(in_features=2048, out_features=5, bias=True)]

    model = resnet_models[0]
    model.fc = resnet_fcs[0]
    model.load_state_dict(checkpoint['current_model'])
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    test_loader = DataLoader(dataset.RetinopathyDataset(mode=dataset.Mode.TEST), batch_size=32, num_workers=4)

    criterion = nn.CrossEntropyLoss().to(device)
    model.to(device)
    val_loss, val_acc, cm = Models.validate(0, test_loader, model, criterion, device)
    cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

    fig, ax = plt.subplots()
    im = ax.imshow(cm, interpolation='nearest', cmap=plt.cm.Blues)
    ax.figure.colorbar(im, ax=ax)

    ax.set(xticks=np.arange(cm.shape[1]),
           yticks=np.arange(cm.shape[0]),
           xticklabels=np.arange(5), yticklabels=np.arange(5),
           title='Normalized confusion matrix',
           ylabel='True label',
           xlabel='Predicted label')

    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")

    fmt = '.2f'
    thresh = cm.max() / 2.
    for i in range(cm.shape[0]):
        for j in range(cm.shape[1]):
            ax.text(j, i, format(cm[i, j], fmt),
                    ha="center", va="center",
                    color="white" if cm[i, j] > thresh else "black")
    fig.tight_layout()
    plt.show()


if __name__ == '__main__':
    plt.figure()
    for pretrain in [True]:
        for find_lr in [True]:
            for resnet in [50]:
                path = '/home/cgilab/working/DeepLearningAndPractice/deeplearningandpractice/lab3/best_models/' + ('pretrain' if pretrain else 'non-pretrain') + '/' + ('find_lr' if find_lr else 'baseline') + '/' + 'resnet' + str(resnet) + '.pth'
                # plot_comparision(path, name = 'resnet' + str(resnet), pretrain=pretrain, find_lr=find_lr, criteria=Criteria.ACCURACY)
                plot_confusion_matrix(path)

    # plt.title('Comparision between models without pretrain')
    # plt.legend()
    # plt.ylabel('Accuracy')
    # plt.xlabel('Epochs')
    # plt.xticks(np.arange(0, 20), np.arange(1, 21))
    # plt.show()
