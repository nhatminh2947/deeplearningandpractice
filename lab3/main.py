import torchvision.models as models
import torch
import torch.nn as nn
import torch.optim as optim
import copy
from torch.utils.data import DataLoader
from lab3 import dataset
from lab3 import Models

num_epochs = 20
batch_size = 8
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
print("Device: {}".format(device))


def save_checkpoint(epoch, model, optimizer, loss_logger, accuracy_logger, best_model):
    state = {'epoch': epoch + 1, 'current_model': model.state_dict(),
             'optimizer': optimizer.state_dict(),
             'loss_logger': loss_logger,
             'accuracy_logger': accuracy_logger,
             'best_models': best_model.state_dict()}

    torch.save(state, model.name + '.pth')


def main():
    test_loader = DataLoader(dataset.RetinopathyDataset(mode=dataset.Mode.TEST), batch_size=batch_size, num_workers=4)
    resnet_models = [Models.resnet18(), Models.resnet50()]
    resnet_names = ['resnet18', 'resnet50']
    resnet_fcs = [nn.modules.Linear(in_features=512, out_features=5, bias=True),
                  nn.modules.Linear(in_features=2048, out_features=5, bias=True)]
    resnet_learning_rate = [1e-4, 1e-5]  # 10-4 10-5
    continue_training = False

    for i, model in enumerate(resnet_models):
        train_loss_logger = []
        train_accuracy_logger = []
        test_loss_logger = []
        test_accuracy_logger = []

        best_model = None
        start_epoch = 0
        optimizer = optim.SGD(model.parameters(), lr=resnet_learning_rate[i], momentum=0.9, weight_decay=5e-4)
        criterion = nn.CrossEntropyLoss().to(device)

        model.name = resnet_names[i]
        model.fc = resnet_fcs[i]

        if continue_training:
            checkpoint = torch.load('./best_models/find_lr/'+model.name+'.pth')
            start_epoch = checkpoint['epoch']
            model.load_state_dict(checkpoint['current_model'])

            train_loss_logger, test_loss_logger = checkpoint['loss_logger']
            train_accuracy_logger, test_accuracy_logger = checkpoint['accuracy_logger']

        print('Training {}'.format(model.name))
        print(model)

        model.to(device)
        best_acc = float('-inf')

        for epoch in range(start_epoch, start_epoch + num_epochs):  # loop over the dataset multiple times
            train_loader = DataLoader(dataset.RetinopathyDataset(mode=dataset.Mode.TRAIN), batch_size=batch_size,
                                      num_workers=4)
            print('\nEpoch [{}/{}]:'.format(epoch + 1, start_epoch + num_epochs))

            train_loss, train_acc = Models.train(epoch, train_loader, model, criterion, optimizer, device)
            print("\n")
            val_loss, val_acc = Models.validate(epoch, test_loader, model, criterion, device)

            train_loss_logger.append(train_loss)
            train_accuracy_logger.append(train_acc)
            test_loss_logger.append(val_loss)
            test_accuracy_logger.append(val_acc)

            if best_acc < val_acc:
                best_model = copy.deepcopy(model)
                best_acc = val_acc

            print('Best accuray: {:.6f}%'.format(best_acc))

            save_checkpoint(epoch, model=model, optimizer=optimizer, loss_logger=(train_loss_logger, test_loss_logger),
                            accuracy_logger=(train_accuracy_logger, test_accuracy_logger), best_model=best_model)

    print('\nFinished Training')


if __name__ == "__main__":
    main()
