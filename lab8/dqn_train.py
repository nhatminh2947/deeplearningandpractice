from itertools import count

import gym
import matplotlib.pyplot as plt
import torch

from lab8 import config
from lab8.Agent import DqnAgent

env = gym.make("CartPole-v0").unwrapped
env._max_episode_steps = 500
observation = env.reset()

agent = DqnAgent(env.observation_space.shape[0], env.action_space.n)

EPISODES = 1000
state = observation

episode_durations = []


def plot_durations():
    plt.figure(2)
    plt.clf()
    durations_t = torch.tensor(episode_durations, dtype=torch.float)
    plt.title('Training...')
    plt.xlabel('Episode')
    plt.ylabel('Duration')
    plt.plot(durations_t.numpy(), label='Duration per episode')

    if durations_t.shape[0] >= 100:
        means = durations_t.unfold(0, 100, 1).mean(1).view(-1)
        means = torch.cat((torch.zeros(99), means))
        plt.plot(means.numpy(), label='Average duration per 100 episodes')

    plt.legend()
    plt.pause(0.001)


best_score = 0.0
for episode in range(config.DQN_TRAINING_EPISODE):
    observation = env.reset()
    state = observation
    for t in count():
        action = agent.take_action(state)
        observation, reward, done, info = env.step(action.item())
        if done:
            reward = -10
        next_state = observation

        agent.remember(state, action, next_state, reward)
        agent.optimize()
        state = next_state

        if done:
            episode_durations.append(t+1)
            plot_durations()
            break

    if episode % config.TARGET_UPDATE == 0:
        avg_score = agent.play(env, 100)
        print('Current avg score: {} | Best score: {}'.format(avg_score, best_score))
        if avg_score > best_score:
            best_score = avg_score
            agent.save('dqn_best_agent.pth')

        agent.update_target_network()

plt.ioff()
plt.show()
env.close()

