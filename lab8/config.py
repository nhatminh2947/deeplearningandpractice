from collections import namedtuple

import torch

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

# DQN Config
Transition = namedtuple('Transition', ('state', 'action', 'next_state', 'reward'))
LEARNING_RATE = 0.0005
EPSILON_START = 1
EPSILON_END = 0.1
EPSILON_DECAY = 0.995
BATCH_SIZE = 128
DQN_TRAINING_EPISODE = 1000
GAMMA = 0.95
DQN_BUFFER_SIZE = 5000
TARGET_UPDATE = 50

# DDPG Config
ACTOR_LR = 0.0001
CRITIC_LR = 0.001
TAU = 0.01
DDPG_BATCH_SIZE = 128
DDPG_BUFFER_SIZE = 10000
DDPG_GAMMA = 0.99
DDPG_TRAINING_EPISODE = 3500

