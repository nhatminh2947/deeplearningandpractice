# -*- coding: utf-8 -*-
import random

import gym
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F

from lab8 import config


class OUNoise(object):
    def __init__(self, action_space, mu=0.0, theta=0.15, max_sigma=0.3, min_sigma=0.3, decay_period=100000):
        self.mu = mu
        self.theta = theta
        self.sigma = max_sigma
        self.max_sigma = max_sigma
        self.min_sigma = min_sigma
        self.decay_period = decay_period
        self.action_dim = action_space.shape[0]
        self.low = action_space.low
        self.high = action_space.high
        self.reset()

    def reset(self):
        self.state = np.ones(self.action_dim) * self.mu

    def evolve_state(self):
        x = self.state
        dx = self.theta * (self.mu - x) + self.sigma * np.random.randn(self.action_dim)
        self.state = x + dx
        return self.state

    def take_action(self, action, t=0):
        ou_state = self.evolve_state()
        self.sigma = self.max_sigma - (self.max_sigma - self.min_sigma) * min(1.0, t / self.decay_period)
        return np.clip(action + ou_state, self.low, self.high)


# https://github.com/openai/gym/blob/master/gym/core.py
class NormalizedEnv(gym.ActionWrapper):
    """ Wrap action """

    def action(self, action):
        act_k = (self.action_space.high - self.action_space.low) / 2.
        act_b = (self.action_space.high + self.action_space.low) / 2.
        return act_k * action + act_b

    def reverse_action(self, action):
        act_k_inv = 2. / (self.action_space.high - self.action_space.low)
        act_b = (self.action_space.high + self.action_space.low) / 2.
        return act_k_inv * (action - act_b)


class ReplayMemory(object):
    def __init__(self, capacity):
        self.capacity = capacity
        self.memory = []
        self.position = 0

    def push(self, *args):
        """Saves a transition."""
        if len(self.memory) < self.capacity:
            self.memory.append(None)
        self.memory[self.position] = config.Transition(*args)
        self.position = (self.position + 1) % self.capacity

    def sample(self, batch_size):
        return random.sample(self.memory, batch_size)

    def __len__(self):
        return len(self.memory)


class DQN(nn.Module):
    def __init__(self, observation_size, hidden_size, outputs):
        super(DQN, self).__init__()
        self.hidden_size = hidden_size
        self.observation_size = observation_size
        self.fc1 = nn.Linear(observation_size, hidden_size)
        self.fc2 = nn.Linear(hidden_size, outputs)

    def forward(self, x):
        x = self.fc1(x)
        x = F.relu(x)
        x = self.fc2(x)

        return x


class Actor(nn.Module):
    def __init__(self, observation_size):
        super(Actor, self).__init__()
        self.fc1 = nn.Linear(observation_size, 400)
        self.fc2 = nn.Linear(400, 300)
        self.fc3 = nn.Linear(300, 1)

    def forward(self, x):
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = torch.tanh(self.fc3(x))

        return x


class Critic(nn.Module):
    def __init__(self, observation_size, action_space):
        super(Critic, self).__init__()
        self.fc1 = nn.Linear(observation_size, 400)
        self.fc2 = nn.Linear(400 + action_space, 300)
        self.fc3 = nn.Linear(300, 1)

    def forward(self, x, action):

        x = F.relu(self.fc1(x))
        # print(torch.cat([x, action.view(-1,1)], 1).shape)
        x = F.relu(self.fc2(torch.cat([x, action], 1)))
        x = self.fc3(x)

        return x


# class Critic(nn.Module):
#     def __init__(self, input_size, hidden_size, output_size):
#         super(Critic, self).__init__()
#         self.linear1 = nn.Linear(input_size, hidden_size)
#         self.linear2 = nn.Linear(hidden_size, hidden_size)
#         self.linear3 = nn.Linear(hidden_size, output_size)
#
#     def forward(self, state, action):
#         """
#         Params state and actions are torch tensors
#         """
#         x = torch.cat([state, action], 1)
#         x = F.relu(self.linear1(x))
#         x = F.relu(self.linear2(x))
#         x = self.linear3(x)
#
#         return x
#
#
# class Actor(nn.Module):
#     def __init__(self, input_size, hidden_size, output_size, learning_rate=3e-4):
#         super(Actor, self).__init__()
#         self.linear1 = nn.Linear(input_size, hidden_size)
#         self.linear2 = nn.Linear(hidden_size, hidden_size)
#         self.linear3 = nn.Linear(hidden_size, output_size)
#
#     def forward(self, state):
#         """
#         Param state is a torch tensor
#         """
#         x = F.relu(self.linear1(state))
#         x = F.relu(self.linear2(x))
#         x = torch.tanh(self.linear3(x))
#
#         return x
