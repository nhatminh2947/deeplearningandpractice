import gym
import matplotlib.pyplot as plt
import numpy as np

from lab8.Agent import DdpgAgent
from lab8.models import NormalizedEnv, OUNoise

env = gym.make("Pendulum-v0")
env = NormalizedEnv(env)
env.reset()
# env.render()

agent = DdpgAgent(3, 1)
noise = OUNoise(env.action_space)
rewards = []
avg_rewards = []
plt.ion()

for episode in range(1000):
    state = env.reset()
    noise.reset()
    episode_reward = 0
    for step in range(500):
        # env.render()
        action = agent.take_action(state)
        action = noise.take_action(action, step)

        observation, reward, done, info = env.step(action)
        # print(observation)

        next_state = observation
        agent.remember(state, action, next_state, reward)

        agent.update_target_network()
        state = next_state
        episode_reward += reward

        if done:
            print("episode: {}, reward: {}, average_reward: {}".format(episode, np.round(episode_reward, decimals=2),
                                                                       np.mean(rewards[-10:])))
            break

    rewards.append(episode_reward)
    avg_rewards.append(np.mean(rewards[-10:]))

plt.figure(2)
plt.plot(rewards)
plt.plot(avg_rewards)
plt.plot()
plt.xlabel('Episode')
plt.ylabel('Reward')
plt.ioff()
plt.show()
env.close()
