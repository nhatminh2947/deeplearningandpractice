import random
from itertools import count

import math
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.autograd import Variable

from lab8 import config
from lab8.models import DQN, ReplayMemory, Actor, Critic


class DqnAgent:
    def __init__(self, observation_size, n_action):
        self.n_action = n_action
        self.q_network = DQN(observation_size, 32, self.n_action).to(config.device)
        self.target_network = DQN(observation_size, 32, self.n_action).to(config.device)
        self.optimizer = optim.RMSprop(self.q_network.parameters(), lr=0.0005)
        self.memory = ReplayMemory(config.DQN_BUFFER_SIZE)
        self.step = 0

    def take_action(self, observation, use_epsilon=True):
        state = torch.tensor([observation], device=config.device, dtype=torch.float)

        if use_epsilon is False:
            with torch.no_grad():
                outputs = self.target_network(state)
                return torch.tensor([[outputs.argmax()]], device=config.device, dtype=torch.long)

        eps_threshold = config.EPSILON_END + (config.EPSILON_START - config.EPSILON_END) * \
                        math.exp(-1. * config.EPSILON_DECAY * self.step)

        self.step += 1
        if random.random() > eps_threshold:
            with torch.no_grad():
                outputs = self.q_network(state)
                return torch.tensor([[outputs.argmax()]], device=config.device, dtype=torch.long)
        else:
            return torch.tensor([[random.randrange(self.n_action)]], device=config.device, dtype=torch.long)

    def update_target_network(self):
        self.target_network.load_state_dict(self.q_network.state_dict())

    def optimize(self):
        if len(self.memory) < config.BATCH_SIZE:
            return

        self.q_network.train()
        transitions = self.memory.sample(config.BATCH_SIZE)
        batch = config.Transition(*zip(*transitions))

        non_final_mask = torch.tensor(tuple(map(lambda s: s is not None,
                                                batch.next_state)), device=config.device, dtype=torch.uint8)
        non_final_next_states = torch.cat([s for s in batch.next_state if s is not None])

        state_batch = torch.cat(batch.state)
        action_batch = torch.cat(batch.action)
        reward_batch = torch.cat(batch.reward)

        state_action_values = self.q_network(state_batch).gather(1, action_batch)

        next_state_values = torch.zeros(config.BATCH_SIZE, device=config.device)
        next_state_values[non_final_mask] = self.target_network(non_final_next_states).max(1)[0].detach()
        expected_state_action_values = (next_state_values * config.GAMMA) + reward_batch
        loss = F.smooth_l1_loss(state_action_values, expected_state_action_values.unsqueeze(1))

        self.optimizer.zero_grad()
        loss.backward()
        self.optimizer.step()

    def remember(self, state, action, next_state, reward):
        state = torch.tensor([state], device=config.device, dtype=torch.float)
        action = torch.tensor([[action]], device=config.device, dtype=torch.long)
        reward = torch.tensor([reward], device=config.device, dtype=torch.float)
        next_state = torch.tensor([next_state], device=config.device, dtype=torch.float)

        self.memory.push(state, action, next_state, reward)

    def save(self, path):
        torch.save(self.target_network.state_dict(), path)

    def play(self, env, n_episodes):
        self.q_network.eval()
        self.target_network.eval()
        with torch.no_grad():
            total_score = 0
            for i in range(n_episodes):
                state = env.reset()
                while True:
                    action = self.take_action(state, use_epsilon=False)
                    observation, reward, done, info = env.step(action.item())
                    total_score += reward
                    state = observation

                    if done:
                        break
        return total_score / n_episodes


class DdpgAgent:
    def __init__(self, observation_size, action_space):
        self.action_space = action_space
        self.actor = Actor(observation_size).to(config.device)
        self.actor_target = Actor(observation_size).to(config.device)
        self.actor_optimizer = optim.Adam(self.actor.parameters(), lr=config.ACTOR_LR)

        self.critic = Critic(observation_size, action_space).to(config.device)
        self.critic_target = Critic(observation_size, action_space).to(config.device)
        self.critic_optimizer = optim.Adam(self.critic.parameters(), lr=config.CRITIC_LR)
        self.step = 0

        self.memory = ReplayMemory(config.DDPG_BUFFER_SIZE)
        self.critic_criterion = nn.MSELoss()

        for target_param, param in zip(self.actor_target.parameters(), self.actor.parameters()):
            target_param.data.copy_(param.data)
        for target_param, param in zip(self.critic_target.parameters(), self.critic.parameters()):
            target_param.data.copy_(param.data)

    def take_action(self, state):
        state = Variable(torch.from_numpy(state).float().unsqueeze(0))
        action = self.actor.forward(state)
        action = action.detach().numpy()[0, 0]
        return action

    def update_target_network(self):
        if len(self.memory) < config.DDPG_BATCH_SIZE:
            return

        transitions = self.memory.sample(config.DDPG_BATCH_SIZE)
        batch = config.Transition(*zip(*transitions))

        state_batch = torch.cat(batch.state)
        action_batch = torch.cat(batch.action)
        reward_batch = torch.cat(batch.reward).view(-1, 1)
        next_state_batch = torch.cat(batch.next_state)
        # print(state_batch.shape)
        # print(action_batch.shape)
        # print(reward_batch.shape)
        # print(next_state_batch.shape)
        # Critic loss
        Qvals = self.critic.forward(state_batch, action_batch)
        next_actions = self.actor_target.forward(next_state_batch)
        next_Q = self.critic_target.forward(next_state_batch, next_actions.detach())
        Qprime = reward_batch + config.DDPG_GAMMA * next_Q

        critic_loss = self.critic_criterion(Qvals, Qprime)

        # print('Qvals.shape: {}'.format(Qvals.shape))
        # print('next_actions.shape: {}'.format(next_actions.shape))
        # print('next_Q.shape: {}'.format(next_Q.shape))
        # print('Qprime.shape: {}'.format(Qprime.shape))

        # Actor loss
        policy_loss = -self.critic.forward(state_batch, self.actor.forward(state_batch)).mean()

        # update networks
        self.actor_optimizer.zero_grad()
        policy_loss.backward()
        self.actor_optimizer.step()

        self.critic_optimizer.zero_grad()
        critic_loss.backward()
        self.critic_optimizer.step()

        # update target networks
        for target_param, param in zip(self.actor_target.parameters(), self.actor.parameters()):
            target_param.data.copy_(param.data * config.TAU + target_param.data * (1.0 - config.TAU))

        for target_param, param in zip(self.critic_target.parameters(), self.critic.parameters()):
            target_param.data.copy_(param.data * config.TAU + target_param.data * (1.0 - config.TAU))

    def remember(self, state, action, next_state, reward):
        state = torch.tensor([state], device=config.device, dtype=torch.float)
        action = torch.tensor([action], device=config.device, dtype=torch.float)
        reward = torch.tensor([reward], device=config.device, dtype=torch.float)
        next_state = torch.tensor([next_state], device=config.device, dtype=torch.float)
        self.memory.push(state, action, next_state, reward)

    def save(self, path):
        torch.save(self.critic.state_dict(), path)

    def play(self, env, n_episodes):
        self.actor.eval()
        self.critic.eval()
        with torch.no_grad():
            total_score = 0
            for i in range(n_episodes):
                state = env.reset()
                for t in count():
                    action = self.take_action(state, use_epsilon=False)
                    observation, reward, done, info = env.step(action.item())
                    total_score += reward
                    state = observation

                    if done or t > 500:
                        break
        return total_score / n_episodes
