# -*- coding: utf-8 -*-

from itertools import count

import gym
import matplotlib.pyplot as plt
import torch

import lab8.config as config
from lab8.models import DQN

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

env = gym.make('CartPole-v0').unwrapped
observation_space = env.observation_space.shape[0]
n_actions = env.action_space.n
agent = DQN(observation_space, 32, n_actions).float().to(device)
agent.load_state_dict(torch.load('./dqn_best_agent_535.pth'))

plt.ion()

episode_durations = []


def plot_durations():
    plt.figure(2)
    plt.clf()
    durations_t = torch.tensor(episode_durations, dtype=torch.float)
    plt.title('Playing...')
    plt.xlabel('Episode')
    plt.ylabel('Duration')
    plt.axhline(y=200, color='r', linestyle='-', label='200 duration average goal')
    plt.plot(durations_t.numpy(), label='Duration per episode')
    plt.legend()
    # Take 100 episode averages and plot them too

    if durations_t.shape[0] >= 100:
        means = durations_t.unfold(0, 100, 1).mean(1).view(-1)
        means = torch.cat((torch.zeros(99), means))
        plt.plot(means.numpy())

    plt.pause(0.001)


def eval_agent(agent, render=False):
    score = 0.0
    observation = env.reset()
    state = torch.tensor([observation], device=config.device, dtype=torch.float)

    with torch.no_grad():
        for t in count():
            if render:
                env.render()
            outputs = agent(state).detach()
            action = torch.tensor([[outputs.argmax()]], device=config.device, dtype=torch.long)

            observation, r, done, _ = env.step(action.item())
            state = torch.tensor([observation], device=config.device, dtype=torch.float)
            # print(r)
            score += r

            # print('score : {:.0f}, action : {}'.format(score, action))
            if done:
                episode_durations.append(t + 1)
                break

    return score


score = 0
for i in range(100):
    score += eval_agent(agent)

print('Average score {}'.format(score / 100.0))
plt.ioff()
plt.show()
env.close()
