from lab1 import NeuralNetwork
from lab1 import Utils

if __name__ == '__main__':
    n = 100
    # X, y = Utils.generate_linear()
    X, y = Utils.generate_XOR_easy()

    # nn = NeuralNetwork.load_model('model.txt')
    nn = NeuralNetwork.NeuralNetwork(input_size=2, hidden_layer_sizes=[10, 10], output_layer_size=1)
    loss_values, validation_loss_values = nn.training(X, y, epochs=100000, learning_rate=0.01,
                                                      batch_size=min(32, X.shape[0]), validation=0)
    nn.save_model("xor_model.txt")

    y_pred = nn.predict(X)
    print(nn.feed_forward(X)[-1])
    Utils.show_learning_curve(loss_values)
    Utils.show_result(X, y, y_pred, nn)
