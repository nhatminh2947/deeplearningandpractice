import numpy as np
import Utils


class NeuralNetwork:
    def __init__(self, input_size=2, hidden_layer_sizes=[], output_layer_size=1):
        self.input_layer_size = input_size
        self.hidden_layer_sizes = hidden_layer_sizes
        self.output_layer_size = output_layer_size
        self.w = []
        self.z = []

        self.w.append(np.random.rand(input_size, hidden_layer_sizes[0]))
        self.z.append(np.random.rand(input_size, hidden_layer_sizes[0]))

        for i in range(1, len(hidden_layer_sizes)):
            self.w.append(np.random.rand(hidden_layer_sizes[i - 1], hidden_layer_sizes[i]))
            self.z.append(np.random.rand(hidden_layer_sizes[i - 1], hidden_layer_sizes[i]))

        self.w.append(np.random.rand(hidden_layer_sizes[-1], output_layer_size))
        self.z.append(np.random.rand(hidden_layer_sizes[-1], output_layer_size))

    def set_weights(self, weights):
        self.w = weights

    def set_input_size(self, input_layer_size):
        self.input_layer_size = input_layer_size

    def set_output_size(self, output_layer_size):
        self.output_layer_size = output_layer_size

    def save_model(self, path):
        with open(path, 'w') as model:
            model.write(str(self.input_layer_size) + '\n')
            model.write(' '.join(str(layer_size) for layer_size in self.hidden_layer_sizes) + '\n')
            model.write(str(self.output_layer_size) + '\n')

            for weights in self.w:
                for row in weights:
                    model.write(' '.join(str(w) for w in row) + '\n')

    def feed_forward(self, X):
        z = [None] * len(self.w)
        z[0] = Utils.sigmoid(X @ self.w[0])
        for i in range(1, len(self.hidden_layer_sizes)):
            z[i] = Utils.sigmoid(z[i-1] @ self.w[i])

        z[-1] = Utils.sigmoid(z[-2] @ self.w[-1])

        return z

    def predict(self, X):
        y_pred = self.feed_forward(X)[-1]
        return np.where(y_pred > 0.5, 1, 0)

    def training(self, X, y_hat, learning_rate=0.001, batch_size=32, epochs=100000, validation=0):
        X_validation = []
        y_validation = []
        if validation != 0:
            index = np.random.choice(X.shape[0], int(X.shape[0] * validation), replace=False)
            mask = np.zeros(X.shape[0], dtype=bool)
            mask[index] = 1
            X_validation = X[~mask]
            y_validation = y_hat[~mask]
            X = X[mask]
            y_hat = y_hat[mask]
        loss_values = []
        validation_loss_values = []
        total_loss = 0
        for epoch in range(epochs):
            total_loss = 0
            for batch in range(0, len(X), batch_size):
                x_train = X[batch:batch + batch_size]
                target = y_hat[batch:batch + batch_size]
                z = self.feed_forward(x_train)
                output = z[-1]
                error = output - target

                delta = error * Utils.derivative_sigmoid(output)
                dEdw = z[-2].T.dot(delta)
                self.w[-1] = self.w[-1] - learning_rate * dEdw

                for i in reversed(range(1, len(self.hidden_layer_sizes))):
                    error = delta.dot(self.w[i+1].T)
                    delta = error * Utils.derivative_sigmoid(z[i])
                    dEdw = z[i-1].T.dot(delta)
                    self.w[i] = self.w[i] - learning_rate * dEdw

                error = delta.dot(self.w[1].T)
                delta = error * Utils.derivative_sigmoid(z[0])
                dEdw = x_train.T.dot(delta)
                self.w[0] = self.w[0] - learning_rate * dEdw

                total_loss += loss_function(output, target)

            if validation != 0:
                validation_loss_values.append(self.validate(X_validation, y_validation))

            loss_values.append(total_loss)
            if epoch % 5000 == 0:
                print('epoch {} loss: {}'.format(epoch, total_loss))
        print('Final loss: {}'.format(total_loss))

        return loss_values, validation_loss_values

    def validate(self, X, y_hat):
        return loss_function(self.feed_forward(X)[-1], y_hat)


def load_model(path):
    with open(path, 'r') as file:
        input_layer_size = int(file.readline().rstrip('\n'))
        hidden_layer_sizes = [int(layer_size) for layer_size in file.readline().rstrip('\n').split(' ')]
        output_layer_size = int(file.readline().rstrip('\n'))

        weights = []
        weight = []
        for i in range(input_layer_size):
            weight.append([float(w) for w in file.readline().rstrip('\n').split(' ')])
        weights.append(weight)

        weight = []
        for i in range(len(hidden_layer_sizes)-1):
            for j in range(hidden_layer_sizes[i]):
                weight.append([float(w) for w in file.readline().rstrip('\n').split(' ')])
            weights.append(weight)

        weight = []
        for i in range(hidden_layer_sizes[-1]):
            weight.append([float(w) for w in file.readline().rstrip('\n').split(' ')])
        weights.append(weight)

        nn = NeuralNetwork(input_size=input_layer_size, hidden_layer_sizes=hidden_layer_sizes, output_layer_size=output_layer_size)
        nn.set_weights(weights)

    return nn


def loss_function(output, target):
    return np.mean(((output - target) ** 2) / 2)