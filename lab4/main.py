import numpy as np
from lab4 import Model
import random


def binary(x):
    bx = np.zeros(8)
    i = 7
    while x != 0:
        bx[i] = x % 2
        x //= 2
        i -= 1
    return np.flip(bx)


def data_generator():
    size = 256
    x = np.zeros((size * size, 8, 2), dtype=int)
    y = np.zeros((size * size, 8), dtype=int)
    for i in range(size):
        for j in range(size-i):
            if i + j >= size:
                continue

            bi = binary(i)
            bj = binary(j)

            # print(bin(i + j))

            bk = binary(i + j)
            # print(bk)
            x[i * size + j, :, 0] = bi
            x[i * size + j, :, 1] = bj
            y[i * size + j] = bk

    return x, y


if __name__ == '__main__':
    x, y = data_generator()

    rnn = Model.RNN()
    rnn.train(x, y)

    for i in range(10):
        r = random.randint(1000, 1200)
        print(x[r])
        print(y[r])
        print(rnn.predict(x[r]))
