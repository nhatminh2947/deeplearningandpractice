import numpy as np
import random


class Sigmoid:
    def forward(self, x):
        return 1 / (1 + np.exp(-x))

    def backward(self, x):
        output = self.forward(x)
        return (1 - output) * output

    def diff(self, x, y):
        probs = self.forward(x)
        probs[y] -= 1.0
        return probs


class Tanh:
    def forward(self, x):
        return np.tanh(x)

    def backward(self, x):
        return 1 - self.forward(x) ** 2


class RNN:
    def __init__(self, input_dim=2, hidden_dim=16, output_dim=1, num_layers=8):
        self.hidden_dim = hidden_dim
        self.input_dim = input_dim
        self.output_dim = output_dim
        self.num_layers = num_layers
        self.U = np.random.uniform(-np.sqrt(1. / hidden_dim), np.sqrt(1. / hidden_dim), (hidden_dim, 2))
        self.W = np.random.uniform(-np.sqrt(1. / hidden_dim), np.sqrt(1. / hidden_dim), (hidden_dim, hidden_dim))
        self.V = np.random.uniform(-np.sqrt(1. / hidden_dim), np.sqrt(1. / hidden_dim), (1, hidden_dim))
        self.b = np.random.uniform(-np.sqrt(1. / hidden_dim), np.sqrt(1. / hidden_dim), (hidden_dim, 1))
        self.c = np.random.uniform(-np.sqrt(1. / hidden_dim), np.sqrt(1. / hidden_dim), (1, 1))
        self.output = np.zeros((num_layers, 1))
        self.h = np.zeros((self.num_layers + 1, self.hidden_dim, 1))
        self.h[-1] = np.zeros((self.hidden_dim, 1))

    def sgd_step(self, x, y, learning_rate):
        dU, dW, dV, db, dc = self.bptt(x, y)
        self.U -= learning_rate * dU
        self.V -= learning_rate * dV
        self.W -= learning_rate * dW
        self.b -= learning_rate * db
        self.c -= learning_rate * dc

    def train(self, X, Y, learning_rate=0.05, epochs=100000):
        num_examples_seen = 0

        accuracy = np.array([])
        for epoch in range(epochs):
            i = random.randint(0, len(X) - 1)
            self.forward_propagation(X[i])
            predicted = np.array([0 if o < 0.5 else 1 for o in self.output])
            error = self.error(Y[i], predicted)
            accuracy = np.append(accuracy, 1 if error == 0 else 0)

            if accuracy.size > 1000:
                accuracy = np.delete(accuracy, 0)

            print('Error: {} Accuracy: {:.3f}%'.format(error, accuracy.sum() * 100 / 1000))

            self.sgd_step(X[i], Y[i], learning_rate)
            num_examples_seen += 1

    def error(self, target, predicted):
        e = 0
        for i in np.array((target == predicted)):
            if not i:
                e += 1

        return e

    def forward_propagation(self, x):
        tanh = Tanh()
        sigmoid = Sigmoid()

        for t in range(0, self.num_layers):
            a = self.b + self.W.dot(np.reshape(self.h[t - 1], (self.hidden_dim, 1))) + self.U.dot(
                np.reshape(x[t], (self.input_dim, 1)))
            self.h[t] = tanh.forward(a)
            self.output[t] = sigmoid.forward(self.c + self.V.dot(self.h[t]))

        return self.h, self.output

    def predict(self, x):
        h, output = self.forward_propagation(x)
        return [0 if o < 0.5 else 1 for o in output]

    def bptt(self, x, y):
        dLdU = np.zeros(self.U.shape)
        dLdV = np.zeros(self.V.shape)
        dLdW = np.zeros(self.W.shape)
        dLdh = np.zeros(self.h.shape)
        dLdb = np.zeros(self.b.shape)
        dLdc = np.zeros(self.c.shape)

        dLdo = self.output - np.reshape(y, (len(y), 1))
        dLdo = np.append(dLdo, [0])

        dLdo = np.reshape(dLdo, (len(dLdo), 1))

        H = np.zeros((self.num_layers + 1, self.hidden_dim, self.hidden_dim))
        for t in range(self.num_layers + 1):
            H[t] = np.diag(np.reshape(1 - self.h[t] * self.h[t], self.hidden_dim))

        for t in reversed(range(self.num_layers)):
            dLdh[t] = self.W.T.dot(H[t + 1]).dot(dLdh[t+1]) + np.reshape(self.V.T.dot(dLdo[t]), (self.hidden_dim, 1))
            dLdW += np.clip(H[t].dot(dLdh[t]).dot(self.h[t - 1].T), -0.01, 0.01)
            dLdU += np.reshape(H[t].dot(self.V.T.dot(dLdo[t])), (self.hidden_dim, 1)).dot(
                np.reshape(x[t], (self.input_dim, 1)).T)
            dLdV += dLdo[t].dot(self.h[t].T)
            dLdb += np.reshape(H[t].dot(self.V.T.dot(dLdo[t])), (self.hidden_dim, 1))
            dLdc += dLdo[t]

        return dLdU, dLdW, dLdV, dLdb, dLdc

    def calculate_loss(self, X, Y):
        return -self.calculate_total_loss(X, Y)

    def calculate_total_loss(self, x, y):
        loss = 0.0
        h, output = self.forward_propagation(x)
        for i in range(len(x)):
            loss += y[i] * (np.log(output[i]))
        return loss
