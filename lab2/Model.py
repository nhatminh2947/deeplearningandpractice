import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import numpy as np
import math
import torch.utils.data as data_utils
import matplotlib.pyplot as plt


class DeepConvNet(nn.Module):
    def __init__(self, activation):
        super(DeepConvNet, self).__init__()
        if isinstance(activation, nn.ELU):
            self.name = 'deepconv_elu.pth'
        elif isinstance(activation, nn.ReLU):
            self.name = 'deepconv_ReLu.pth'
        elif isinstance(activation, nn.LeakyReLU):
            self.name = 'deepconv_LeakyReLU.pth'

        p = 0.75

        self.first_conv = nn.Sequential(
            nn.Conv2d(1, 25, kernel_size=(1, 5), padding=0, bias=True),
            nn.Conv2d(25, 25, kernel_size=(2, 1), padding=0, bias=True),
            nn.BatchNorm2d(25, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True),
            activation,
            nn.MaxPool2d(kernel_size=(1, 2), stride=(1, 2), padding=0),
            nn.Dropout(p=p)
        )
        self.second_conv = nn.Sequential(
            nn.Conv2d(25, 50, kernel_size=(1, 5), stride=(1, 1), padding=0, bias=True),
            nn.BatchNorm2d(50, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True),
            activation,
            nn.MaxPool2d(kernel_size=(1, 2), stride=(1, 2), padding=0),
            nn.Dropout(p=p)
        )
        self.third_conv = nn.Sequential(
            nn.Conv2d(50, 100, kernel_size=(1, 5), stride=(1, 1), padding=0, bias=True),
            nn.BatchNorm2d(100, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True),
            activation,
            nn.MaxPool2d(kernel_size=(1, 2), stride=(1, 2), padding=0),
            nn.Dropout(p=p)
        )
        self.fourth_conv = nn.Sequential(
            nn.Conv2d(100, 200, kernel_size=(1, 5), stride=(1, 1), padding=0, bias=True),
            nn.BatchNorm2d(200, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True),
            activation,
            nn.MaxPool2d(kernel_size=(1, 2), stride=(1, 2), padding=0),
            nn.Dropout(p=p)
        )
        self.classify = nn.Sequential(
            nn.Linear(in_features=8600, out_features=2, bias=True)
        )

    def forward(self, x):
        x = self.first_conv(x)
        x = self.second_conv(x)
        x = self.third_conv(x)
        x = self.fourth_conv(x)
        x = x.reshape(-1, 8600)
        return self.classify(x)


class EEGModel(nn.Module):
    def __init__(self, activation):
        super(EEGModel, self).__init__()
        if isinstance(activation, nn.ELU):
            self.name = 'model_elu.pth'
        elif isinstance(activation, nn.ReLU):
            self.name = 'model_ReLu.pth'
        elif isinstance(activation, nn.LeakyReLU):
            self.name = 'model_LeakyReLU.pth'
        p = 0.75
        self.firstconv = nn.Sequential(
            nn.Conv2d(1, 16, kernel_size=(1, 51), stride=(1, 1), padding=(0, 25), bias=False),
            nn.BatchNorm2d(16, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        )
        self.depthwiseConv = nn.Sequential(
            nn.Conv2d(16, 32, kernel_size=(2, 1), stride=(1, 1), groups=16, bias=False),
            nn.BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True),
            activation,
            nn.AvgPool2d(kernel_size=(1, 4), stride=(1, 4), padding=0),
            nn.Dropout(p=p)
        )
        self.separableConv = nn.Sequential(
            nn.Conv2d(32, 32, kernel_size=(1, 15), stride=(1, 1), padding=(0, 7), bias=False),
            nn.BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True),
            activation,
            nn.AvgPool2d(kernel_size=(1, 8), stride=(1, 8), padding=0),
            nn.Dropout(p=p)
        )
        self.classify = nn.Sequential(
            nn.Linear(in_features=736, out_features=2, bias=True)
        )

    def forward(self, x):
        x = self.firstconv(x)
        x = self.depthwiseConv(x)
        x = self.separableConv(x)
        x = x.reshape(-1, 736)
        return self.classify(x)


def train(train_loader, model, criterion, optimizer, device, log):
    model.train()
    correct = 0
    total_loss = 0
    for i, (inputs, targets) in enumerate(train_loader):
        inputs = inputs.to(device)
        targets = targets.to(device)

        outputs = model(inputs)
        loss = criterion(outputs, targets)

        _, predicted = torch.max(outputs.data, 1)
        correct += (predicted == targets).sum().item()
        total_loss += loss.item()

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
    if log:
        print('Training Loss: {:.6f}, Accuracy: {:.4f}%'
              .format(total_loss / len(train_loader), (correct / len(train_loader.dataset)) * 100))

    return total_loss / len(train_loader), (correct / len(train_loader.dataset)) * 100


def validate(val_loader, model, criterion, device, log):
    model.eval()
    correct = 0
    total_loss = 0

    for i, (inputs, targets) in enumerate(val_loader):
        with torch.no_grad():
            inputs = inputs.to(device)
            targets = targets.to(device)
            outputs = model(inputs)
            loss = criterion(outputs, targets)
            total_loss += loss.item()

            _, predicted = torch.max(outputs.data, 1)
            correct += (predicted == targets).sum().item()
    if log == True:
        print('Test set: Loss: {:.6f} Accuracy: {}/{} ({:.5f}%)'
              .format(total_loss / len(val_loader),
                      correct, len(val_loader.dataset), 100. * correct / len(val_loader.dataset)))

    return total_loss / len(val_loader), 100. * correct / len(val_loader.dataset)
