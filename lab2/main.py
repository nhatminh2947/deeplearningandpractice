from lab2 import dataloader, Model
import torch
import torch.nn as nn
import torch.optim as optim
import torch.utils.data as data_utils
import matplotlib.pyplot as plt
from enum import Enum


class Criteria(Enum):
    ACCURACY = 'Accuracy'
    LOSS = 'Loss'


def plot_figure(model_name, loss, acc):
    fig, ax1 = plt.subplots()

    color = 'tab:red'
    ax1.set_xlabel('epochs')
    ax1.set_ylabel('loss', color=color)
    ax1.plot(loss, color=color, label='loss')

    ax2 = ax1.twinx()

    color = 'tab:blue'
    ax2.set_ylabel('accuracy', color=color)
    ax2.plot(acc, color=color, label='accuracy')

    ylim1 = ax1.get_ylim()
    len1 = ylim1[1] - ylim1[0]
    yticks1 = ax1.get_yticks()
    rel_dist = [(y - ylim1[0]) / len1 for y in yticks1]
    ylim2 = ax2.get_ylim()
    len2 = ylim2[1] - ylim2[0]
    yticks2 = [ry * len2 + ylim2[0] for ry in rel_dist]

    ax2.set_yticks(yticks2)
    ax2.set_ylim(ylim2)
    ax1.legend(loc='upper left')
    ax2.legend(loc='upper right')

    fig.tight_layout()
    plt.show()


def plot_comparision(criteria):
    plt.figure()

    for model in ['elu', 'LeakyReLU', 'ReLu']:
        loss_acc = torch.load('./non/model_' + model + '.pth_loss_acc.log')

        train = []
        val = []
        if criteria == Criteria.LOSS:
            train = loss_acc['train_losses']
            val = loss_acc['val_losses']
        elif criteria == Criteria.ACCURACY:
            train = loss_acc['train_accs']
            val = loss_acc['val_accs']

        plt.plot(train[:10000], label=model + '_train')
        plt.plot(val[:10000], label=model + '_test')

    plt.title('Default EEGNet with ELU')
    plt.legend()
    plt.ylabel(criteria.name)
    plt.xlabel('Epochs')
    plt.show()

    plt.figure()

    for model in ['elu', 'LeakyReLU', 'ReLu']:
        loss_acc = torch.load('./non/deepconv_' + model + '.pth_loss_acc.log')

        train = []
        val = []
        if criteria == Criteria.LOSS:
            train = loss_acc['train_losses']
            val = loss_acc['val_losses']
        elif criteria == Criteria.ACCURACY:
            train = loss_acc['train_accs']
            val = loss_acc['val_accs']

        plt.plot(train[:10000], label=model + '_train', markevery=20)
        plt.plot(val[:10000], label=model + '_test', markevery=20)


    plt.title('Activation function comparision (DeepConvNet)')
    plt.legend()
    plt.ylabel(criteria.name)
    plt.xlabel('Epochs')
    plt.show()


def test_model():
    x_train, y_train, x_test, y_test = dataloader.read_bci_data()
    x_train = torch.from_numpy(x_train).float()
    y_train = torch.from_numpy(y_train).long()
    x_test = torch.from_numpy(x_test).float()
    y_test = torch.from_numpy(y_test).long()
    batch_size = 64

    train = data_utils.TensorDataset(x_train, y_train)
    train_loader = data_utils.DataLoader(train, batch_size=batch_size, shuffle=True)

    test = data_utils.TensorDataset(x_test, y_test)
    test_loader = data_utils.DataLoader(test, batch_size=batch_size, shuffle=True)

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    for activ in [nn.ELU(), nn.ReLU(), nn.LeakyReLU()]:
        model = Model.EEGModel(activ)
        model.load_state_dict(torch.load('./non/'+model.name))
        criterion = nn.CrossEntropyLoss().to(device)
        model.to(device)

        print(model)

        train_loss, train_acc = Model.validate(train_loader, model, criterion, device, False)
        val_loss, val_acc = Model.validate(test_loader, model, criterion, device, False)

        print('Train result: train loss: {} \ttrain accuracy {}'.format(train_loss, train_acc))
        print('Test result: test loss: {} \ttest accuracy {}'.format(val_loss, val_acc))


def main():
    x_train, y_train, x_test, y_test = dataloader.read_bci_data()
    x_train = torch.from_numpy(x_train).float()
    y_train = torch.from_numpy(y_train).long()
    x_test = torch.from_numpy(x_test).float()
    y_test = torch.from_numpy(y_test).long()

    num_epochs = 10000
    batch_size = 64

    train = data_utils.TensorDataset(x_train, y_train)
    train_loader = data_utils.DataLoader(train, batch_size=batch_size, shuffle=True)

    test = data_utils.TensorDataset(x_test, y_test)
    test_loader = data_utils.DataLoader(test, batch_size=batch_size, shuffle=True)

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    print("Device: {}".format(device))

    for activ in [nn.ELU(), nn.ReLU(), nn.LeakyReLU()]:
        train_losses = []
        train_accs = []
        val_losses = []
        val_accs = []

        model = Model.EEGModel(activ)
        model.to(device)
        print('Training model: {}'.format(model.name))
        print(model)

        optimizer = optim.Adam(model.parameters(), lr=0.001)
        criterion = nn.CrossEntropyLoss().to(device)

        best_acc = float('-inf')

        for epoch in range(num_epochs):  # loop over the dataset multiple times
            print('\nEpoch [{}/{}]:'.format(epoch + 1, num_epochs))

            train_loss, train_acc = Model.train(train_loader, model, criterion, optimizer, device, True)
            val_loss, val_acc = Model.validate(test_loader, model, criterion, device, True)

            train_losses.append(train_loss)
            train_accs.append(train_acc)
            val_losses.append(val_loss)
            val_accs.append(val_acc)
            if val_acc > best_acc:
                torch.save(model.state_dict(), './non/{}'.format(model.name))
                best_acc = val_acc

            print('Best accuray: {:.6f}%'.format(best_acc))

        torch.save({
            'train_losses': train_losses,
            'train_accs': train_accs,
            'val_losses': val_losses,
            'val_accs': val_accs
        }, './non/'+model.name + '_loss_acc.log')

    print('\nFinished Training')


if __name__ == "__main__":
    #test_model()
    main()